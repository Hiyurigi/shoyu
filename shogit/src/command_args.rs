#[allow(dead_code)]
#[derive(Debug)]
pub enum Args {
    Symrefs,
    Peel,
    RefPrefix(String),
    Unborn,
    Want(String),
    Have(String),
    Done,
    ThinPack,
    NoProgress,
    IncludeTag,
    OfsDelta,
    Shallow(String),
    Deepen(String),
    DeepenRelative,
    DeepenSince(String),
    DeepenNot(String),
    Filter(String),
    WantRef(String),
    SidebandAll,
    PackfileUris(String),
    WaitForDone,
    Size,
    Oid(String)
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct CommandArgsList {
    args: Vec<Args>
}

#[allow(dead_code)]
impl CommandArgsList {
    pub fn from(args: Vec<Args>) -> Self {
        Self{args}
    }
    pub fn build(&self) -> String {
        use Args::*;
        let mut compiled = vec![];
        for x in &self.args {
            compiled.push(match &x {
                Symrefs => self.compile("symrefs",None),
                Peel => self.compile("peel",None),
                RefPrefix(prefix) => self.compile("ref-prefix",Some(prefix)),
                Unborn => self.compile("unborn",None),
                Want(oid) => self.compile("want",Some(oid)),
                Have(oid) => self.compile("have",Some(oid)),
                Done => self.compile("done",None),
                ThinPack => self.compile("thin-pack",None),
                NoProgress => self.compile("no-progress",None),
                IncludeTag => self.compile("include-tag",None),
                OfsDelta => self.compile("ofs-delta",None),
                Shallow(oid) => self.compile("shallow",Some(oid)),
                Deepen(depth) => self.compile("deepen",Some(depth)),
                DeepenRelative => self.compile("deepen-relative",None),
                DeepenSince(timestamp) => self.compile("deepen-since",Some(timestamp)),
                DeepenNot(rev) => self.compile("deepen-not",Some(rev)),
                Filter(filter_spec) => self.compile("filter",Some(filter_spec)),
                WantRef(refs) => self.compile("want-ref",Some(refs)),
                SidebandAll => self.compile("sideband-all",None),
                PackfileUris(clsp) => self.compile("packfile-uris",Some(clsp)),
                WaitForDone => self.compile("wait-for-done",None),
                Size => self.compile("size",None),
                Oid(oid) => self.compile("oid",Some(oid))
            });
        }
        compiled.join("")
    }
    fn compile(&self, key: &str, value: Option<&String>) -> String {
        match value {
            Some(data) => format!("{:04x}{} {}\n", 6 + key.len() + data.len(), key, data),
            None => format!("{:04x}{}\n", 5 + key.len(), key)
        }
    }
}


// impl CommandArgs for Args {
//     fn build(&self) -> String{
        // match &self {
        //     Symrefs => self.compile("symrefs",None),
        //     Peel => self.compile("peel",None),
        //     RefPrefix(prefix) => self.compile("ref-prefix",Some(prefix)),
        //     Unborn => self.compile("unborn",None),
        //     Self::Want(oid) => self.compile("want",Some(oid)),
        //     Self::Have(oid) => self.compile("have",Some(oid)),
        //     Done => self.compile("done",None),
        //     ThinPack => self.compile("thin-pack",None),
        //     NoProgress => self.compile("no-progress",None),
        //     IncludeTag => self.compile("include-tag",None),
        //     OfsDelta => self.compile("ofs-delta",None),
        //     Self::Shallow(oid) => self.compile("shallow",Some(oid)),
        //     Self::Deepen(depth) => self.compile("deepen",Some(depth)),
        //     DeepenRelative => self.compile("deepen-relative",None),
        //     Self::DeepenSince(timestamp) => self.compile("deepen-since",Some(timestamp)),
        //     Self::DeepenNot(rev) => self.compile("deepen-not",Some(rev)),
        //     Self::Filter(filter_spec) => self.compile("filter",Some(filter_spec)),
        //     Self::WantRef(refs) => self.compile("want-ref",Some(refs)),
        //     SidebandAll => self.compile("sideband-all",None),
        //     Self::PackfileUris(clsp) => self.compile("packfile-uris",Some(clsp)),
        //     WaitForDone => self.compile("wait-for-done",None),
        //     Size => self.compile("size",None),
        //     Self::Oid(oid) => self.compile("oid",Some(oid))
        // }
//         // format!("{:04x}{}", 4 + str.len(), str)
//     }
    // fn compile(&self, key: &str, value: Option<&String>) -> String {
    //     match value {
    //         Some(data) => format!("{:04x}{} {}\n", 6 + key.len() + data.len(), key, data),
    //         None => format!("{:04x}{}\n", 5 + key.len(), key)
    //     }
    // }
// }