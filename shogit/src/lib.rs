mod git_request;
mod parser;
mod command_req;
mod capability;
mod capability_list;
mod command_args;
use capability::{Capability};
use git_request::GitRequest;
use command_args::{CommandArgsList, Args};
use capability_list::{CapabilityList};
use std::{collections::BTreeMap};
use command_req::{CommandReq};
use bytes::Bytes;
use shoyu_core::{declare_plugin, Plugin};

#[allow(dead_code)]
pub struct Git {
    capability_list: BTreeMap<String, Vec<String>>,
    request: GitRequest,
}

impl Git {
    pub fn new(url: &str) -> Self{
        let btree = BTreeMap::from([
            (String::from("agent"), vec![String::from("shoyu/0.0.1")]),
            (String::from("object-format"), vec![String::from("sha1")])
        ]);
        let mut req = GitRequest::new(url);
        let query = vec![Some("service=git-upload-pack")];
        let mut resp: &[u8] = &req.get("/info/refs", query);
        println!("{:?}", resp);
        parser::parse(&mut resp);
        Self { capability_list: btree, request: req }
    }
}

pub fn get(url: &str) -> String {
    let btree = BTreeMap::from([
        ("agent", "shoyu/0.0.1"),
        ("object-format", "sha1")
    ]);
    let args = CommandArgsList::from(vec![
        Args::ThinPack,
        Args::NoProgress,
        Args::OfsDelta,
        Args::Want(String::from("30a07f9c60f0b4a363ed12307784c3792eb0d255"))
    ]);

    // ]);
    // let args = CommandArgsList::from(vec![
    //     Args::Peel,
    //     Args::Symrefs,
    //     Args::Unborn,
    //     Args::RefPrefix(String::from("HEAD")),
    //     Args::RefPrefix(String::from("refs/heads/")),
    //     Args::RefPrefix(String::from("refs/tags/"))
    // ]);
    let mut result: &[u8] = &git_request::GitRequest::new(url).post_bytes("/git-upload-pack", CommandReq::new("fetch", CapabilityList::from(&btree), args).build());
    parser::parse(&mut result);
    // println!("{:?}", result);
    return String::from("OK");
}

#[derive(Default)]
pub struct Shogit;


impl Plugin for Shogit {
    type PluginType = Shogit;
    fn new(&self) -> Box<Self> {
        Box::new(Self)
    }
    fn name(&self) -> &'static str  {
        "Shogit"
    }
    fn on_plugin_load(&self) {
        dbg!("Plugin loaded");
    }

    fn on_plugin_unload(&self) {
        print!("Injector unloaded");
    }

    fn pre_send(&self, req: &str) {
        Git::new(req);
    }

    fn post_receive(&self, res: &mut str) {
        // debug!("Received Response");
        // debug!("Headers: {:?}", res.headers);
        print!("Injector unloaded");
    }
}

declare_plugin!(Shogit, Shogit::default);