#[derive(Debug)]
pub struct Capability<'a>{
    key: &'a str,
    value: &'a str
}

impl<'a> Capability<'a> {
    pub fn create(key: &'a str, value: &'a str) -> Self{
        Self {
            key,
            value
        }
    }
    pub fn build(&self) -> String{
        let str = [self.key, "=", self.value, "\n"].join("");
        format!("{:04x}{}", 4 + str.len(), str)
    }
}