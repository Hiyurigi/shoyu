use super::CapabilityList;
use super::CommandArgsList;

#[allow(dead_code)]
#[derive(Debug)]
pub struct CommandReq<'a> {
    command: &'a str,
    capability_list: CapabilityList<'a>,
    delim_pkt: &'a str,
    command_args: CommandArgsList,
    flush_pkt: &'a str
}

#[allow(dead_code)]
impl<'a> CommandReq<'a>{
    pub fn new(command: &'a str, capability_list: CapabilityList<'a>, command_args: CommandArgsList) -> Self {
        Self { command, capability_list, delim_pkt: "0001", command_args, flush_pkt: "0000" }
    }
    pub fn build(&self) -> String{
        let mut cmd: Vec<String> = vec![];
        cmd.push(format!("{:04x}command={}\n", 13 + self.command.len(), self.command));
        cmd.push(self.capability_list.build());
        cmd.push(self.delim_pkt.to_owned());
        cmd.push(self.command_args.build());
        cmd.push(self.flush_pkt.to_owned());
        cmd.join("")
    }
}
