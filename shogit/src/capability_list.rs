use super::Capability;
use std::collections::BTreeMap;

pub trait CapabilityListBuilder {
    fn build(&self) -> CapabilityList;
}

#[derive(Debug)]
pub struct CapabilityList<'a> {
    store: Vec<Capability<'a>>
}

#[allow(dead_code)]
impl<'a> CapabilityList<'a> {
    pub fn new() -> Self {
        Self { store: vec![] }
    }
    pub fn from(list: &'a BTreeMap<&str, &str>) -> Self{
        let mut cp = Self{store: vec![]};
        for (key, value) in list {
            cp.store.push(Capability::create(key, value));
        }
        return cp;
    }
    pub fn build(&self) -> String{
        self.store.iter().map(|v| v.build()).collect::<Vec<String>>().join("")
    }
}