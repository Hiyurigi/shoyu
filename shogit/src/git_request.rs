use reqwest::{Client, Url, header::{HeaderMap, HeaderName, HeaderValue}, Method};
use tokio::runtime::Runtime;
use super::Bytes;

pub struct GitRequest{
    rt: Runtime,
    git_url: Url,
    client: Client,
    header: HeaderMap
}

#[allow(dead_code)]
impl GitRequest{
    pub fn new(git_url: &str) -> Self{
        let mut header = HeaderMap::new();
        header.insert(HeaderName::from_bytes(b"Git-Protocol").unwrap(), HeaderValue::from_static("version=2"));
        return Self {
            rt: Runtime::new().unwrap(),
            git_url: Url::parse(&git_url).unwrap(),
            client: Client::builder().https_only(true).user_agent("shoyu/0.0.1").build().unwrap(),
            header
        };
    }
    pub fn get(&mut self, path: &str, query: Vec<Option<&str>>) -> Bytes{
        self.git_url.set_path(&[self.git_url.path(), path].concat());
        for x in query {
            self.git_url.set_query(x);
        }
        return self.rt.block_on(async {self.client.request(Method::GET, self.git_url.to_owned()).headers(self.header.to_owned()).send().await.unwrap().bytes().await.unwrap()});
    }

    pub fn post(&mut self, path: &str, body: String) -> Bytes{
        self.git_url.set_path(&[self.git_url.path(), path].concat());
        return self.rt.block_on(async {self.client.request(Method::POST, self.git_url.to_owned()).headers(self.header.to_owned()).body(body).send().await.unwrap().bytes().await.unwrap()});
    }
    pub fn post_bytes(&mut self, path: &str, body: String) -> Bytes {
        self.git_url.set_path(&[self.git_url.path(), path].concat());
        return self.rt.block_on(async {self.client.request(Method::POST, self.git_url.to_owned()).headers(self.header.to_owned()).body(body).send().await.unwrap().bytes().await.unwrap()});
    }
}