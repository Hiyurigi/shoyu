use std::{i64, str};

pub trait Parser {
    fn parse(gitreq: &mut &str);
}

#[allow(dead_code)]
pub fn parse(gitreq: &mut &[u8]){
    let mut parsed: Vec<&[u8]> = Vec::new();
    while gitreq.len() > 0 {
        // println!("{:?}", str::from_utf8(&gitreq[..4]));
        let size = i64::from_str_radix(str::from_utf8(&gitreq[..4]).unwrap(), 16).unwrap() as usize;
        if size == 0 || size == 1 || size == 2{
            parsed.push(&gitreq[..4]);
            *gitreq = &gitreq[4..];
            continue;
        }
        parsed.push(&gitreq[4..size]);
        *gitreq = &gitreq[size..];
    }
    for i in parsed.into_iter() {
        println!("{:#?}", String::from_utf8_lossy(i));
    }
}

#[allow(dead_code)]
pub fn parse_bytes(gitreq: &mut &[u8]){
    let mut parsed: Vec<&[u8]> = Vec::new();
    while gitreq.len() > 0 {
        // println!("{:?}", str::from_utf8(&gitreq[..4]));
        let size = i64::from_str_radix(str::from_utf8(&gitreq[..4]).unwrap(), 16).unwrap() as usize;
        if size == 0 || size == 1 || size == 2{
            parsed.push(&gitreq[..4]);
            *gitreq = &gitreq[4..];
            continue;
        }
        parsed.push(&gitreq[4..size]);
        *gitreq = &gitreq[size..];
    }
    println!("{:#?}", parsed);
}