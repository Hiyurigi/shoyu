use shoyu_core::plugin_manager;

fn main() {
    let mut pm = plugin_manager::PluginManager::new();
    print!("{:?}", pm.load_plugin("target/debug/shogit.dll"));
    let url = "https://gitee.com/ja-netfilter/ja-netfilter.git";
    pm.pre_send(url);
    pm.unload();
}
