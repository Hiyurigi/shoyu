pub mod plugins;

pub use plugins::plugin_function::PluginFunction;
pub use plugins::plugin_manager::PluginManager;
use libloading::{Library, Symbol};
use std::ffi::OsStr;

pub static CORE_VERSION: &str = env!("CARGO_PKG_VERSION");
pub static RUSTC_VERSION: &str = env!("RUSTC_VERSION");

#[macro_export]
macro_rules! declare_plugin {
    ($plugin_type:ty, $constructor:path) => {
        #[no_mangle]
        pub extern "C" fn _plugin_create() -> *mut dyn $crate::PluginFunction {
            // make sure the constructor is the correct type.
            let constructor: fn() -> $plugin_type = $constructor;

            let object = constructor();
            let boxed: Box<dyn $crate::PluginFunction> = Box::new(object);
            Box::into_raw(boxed)
        }
    };
}

pub trait PluginConstructor{
    fn construct() -> Self;
}