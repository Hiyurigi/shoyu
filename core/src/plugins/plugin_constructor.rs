pub trait PluginConstructor{
    fn new() -> Self;
}