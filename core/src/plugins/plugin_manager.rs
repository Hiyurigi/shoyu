use crate::PluginFunction;
use crate::PluginConstructor;
use crate::{Symbol, Library};
use crate::{OsStr};
use std::error::Error;
use std::rc::Rc;

type Pc = dyn PluginConstructor;
pub struct PluginManager {
    plugins: Vec<Box<Pc>>,
    loaded_libraries: Vec<Rc<Library>>,
}

impl Drop for PluginManager {
    fn drop(&mut self) {
        if !self.plugins.is_empty() || !self.loaded_libraries.is_empty() {
            self.unload();
        }
    }
}

#[allow(dead_code)]
impl PluginManager {
    pub fn new() -> PluginManager {
        PluginManager {
            plugins: Vec::new(),
            loaded_libraries: Vec::new(),
        }
    }
    pub fn load_plugin<P: AsRef<OsStr>>(&mut self, filename: P) -> Result<(), Box<dyn Error>> {
        unsafe{
            type PluginCreate = unsafe fn() -> *mut dyn PluginFunction;

            let lib = Rc::new(Library::new(filename.as_ref()).expect("Failed to load"));

            // We need to keep the library around otherwise our plugin's vtable will
            // point to garbage. We do this little dance to make sure the library
            // doesn't end up getting moved.
            self.loaded_libraries.push(lib);

            let lib = self.loaded_libraries.last().unwrap();

            let constructor: Symbol<PluginCreate> = lib.get(b"_plugin_create")?;
            let boxed_raw = constructor();

            let plugin = Box::from_raw(boxed_raw);
            println!("Loaded plugin: {}", plugin.name());
            plugin.on_plugin_load();
            self.plugins.push(plugin);


            Ok(())
        }
    }
    pub fn pre_send(&mut self, request: &str) {
        // debug!("Firing pre_send hooks");

        for plugin in &mut self.plugins {
            // trace!("Firing pre_send for {:?}", plugin.name());
            plugin.pre_send(request);
        }
    }

    /// Iterate over the plugins, running their `post_receive()` hook.
    pub fn post_receive(&mut self, response: &mut str) {
        // debug!("Firing post_receive hooks");

        for plugin in &mut self.plugins {
            // trace!("Firing post_receive for {:?}", plugin.name());
            plugin.post_receive(response);
        }
    }

    /// Unload all plugins and loaded plugin libraries, making sure to fire 
    /// their `on_plugin_unload()` methods so they can do any necessary cleanup.
    pub fn unload(&mut self) {
        // debug!("Unloading plugins");

        for plugin in self.plugins.drain(..) {
            // trace!("Firing on_plugin_unload for {:?}", plugin.name());
            plugin.on_plugin_unload();
        }

        for lib in self.loaded_libraries.drain(..) {
            drop(lib);
        }
    }
}